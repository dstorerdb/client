/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.client.system;

import org.dstorerdb.client.net.Client;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author martin
 */
public class ConsoleClient {
    private final Scanner scanner;
    private Client client;
    private boolean isLoged;

    public ConsoleClient() throws IOException {
        scanner = new Scanner(System.in);
        client = new Client();
        isLoged = false;
    }
    
    public void printReceivMsg(){
        System.out.println(client.getReceivMsg());
    }
    
    public void printReceivMsg(String msg){
        System.out.println(msg);
    }
    
    public void printLine(){
        System.out.print("> ");
    }
    
    public String readLine(){
        return scanner.nextLine();
    }
    
    public void start() throws IOException{
        String cmd;
        String msg;

        while (true) {
            printLine();
            cmd = readLine();
            client.sendMsg(cmd);
            msg = client.getReceivMsg();
            isLoged = msg.contains("Login correcto");
            printReceivMsg(msg);
            if (!isLoged)
                client.reconnect();
            else while (isLoged) {
                printLine();
                cmd = readLine().trim();
                if (!cmd.isEmpty()) {
                    client.sendMsg(cmd);
                    printReceivMsg();
                    if (cmd.equals("close")) {
                        break;
                    }
                }
            }
            if (isLoged) {
                client.closeConnection();
                break;
            }
        }
    }
    
}
