/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.client.net;

import org.dstorerdb.common.system.SysInfo;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author martin
 */
public class Client {
    private Socket sockClient;
    private Sender sender;
    private Receiver receiver;
    
    public Client() throws IOException{
        sockClient = new Socket();
        connectSocket();
        sender = new Sender(sockClient.getOutputStream());
        receiver = new Receiver(sockClient.getInputStream());
    }
    
    public Client(String host) throws IOException{
        this(host, SysInfo.DEFAULT_PORT_1);
    }

    public Client(String host, int port) throws IOException {
        sockClient = new Socket(host, port);
        sender = new Sender(sockClient.getOutputStream());
        receiver = new Receiver(sockClient.getInputStream());
    }

    private void connectSocket(){
        try {
            sockClient.connect(new InetSocketAddress(SysInfo.DEFAULT_HOST, SysInfo.DEFAULT_PORT_1));
        } catch (IOException ex) {
            try {
                sockClient = new Socket();
                sockClient.connect(new InetSocketAddress(SysInfo.LOCALHOST, SysInfo.DEFAULT_PORT_1));
            } catch (IOException ex1) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void reconnect(){
        try {
            closeConnection();
            sockClient = new Socket();
            connectSocket();
            sender = new Sender(sockClient.getOutputStream());
            receiver = new Receiver(sockClient.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeConnection() throws IOException{
        sockClient.shutdownOutput();
        sockClient.shutdownInput();
        sockClient.close();
    }

    public void sendMsg(String data) {
        sender.sendData(data);
    }

    public String getReceivMsg() {
        return receiver.getReceivData();
    }
    
}
