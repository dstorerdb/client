/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.client.net;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dstorerdb.common.interfaces.Transferible;
import org.dstorerdb.common.streams.StringOutputStream;
import org.dstorerdb.common.system.ServerMessages;
import org.dstorerdb.encryptor.exceptions.InvalidTextException;

/**
 *
 * @author martin
 */
public class Sender implements Transferible {
    private final StringOutputStream outputStream;

    public Sender(OutputStream sockStream) {
        outputStream = new StringOutputStream(sockStream);
    }

    @Override
    public void sendData(String data) {
        try {
            outputStream.writeString(data+ ServerMessages.REQUEST_MSG);
        } catch (IOException | InvalidTextException ex) {
            Logger.getLogger(Sender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
